import 'package:flutter/material.dart';

class TextStream {

  Stream<String> getWord() async* {
    final List<String> word = [
      'Message 1',
      'Message 2',
      'Message 3',
      'Message 4',
      'Message 5'
    ];

    yield* Stream.periodic(Duration(seconds: 5), (int t) {
      int index = t % 5;

      return word[index];
    });


  }
}