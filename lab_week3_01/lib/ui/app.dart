import 'package:flutter/material.dart';
import '../stream/word_stream.dart';

// class App extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return HomePageStream();
//   }
//
// }

class App extends StatefulWidget {


  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }

}

class HomePageState extends State<StatefulWidget> {
  Color bgTextColor = Colors.blueGrey;
  String word = '';
  List<String> message = ['','','','','',''];
  TextStream textStream = TextStream();
  late int count = 0;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stream Word',

      home: Scaffold(
        appBar: AppBar(title: Text('Message Stream'),),
        backgroundColor: bgTextColor,
        body: Center(
            child:Column(
                children:[
                  Text(message[0], style: TextStyle(fontSize: 50)),
                  Text(message[1], style: TextStyle(fontSize: 50)),
                  Text(message[2], style: TextStyle(fontSize: 50)),
                  Text(message[3], style: TextStyle(fontSize: 50)),
                  Text(message[4], style: TextStyle(fontSize: 50)),

                ]

            )
        ),
        floatingActionButton: FloatingActionButton(
          child: Text('C'),
          onPressed: () {
            changeWord();
          },
        ),
      ),
    );
  }

  changeWord() async {
    textStream.getWord().listen((eventEnglish) {
      setState(() {
        print(eventEnglish);
        bgTextColor = Colors.green;
        word = eventEnglish.toLowerCase();
        message[count] = word;
        if (count == eventEnglish.length+1)
          return null;
        else count +=1;


      });
    });
  }
}

